<!doctype html> <!--HTML5 tag-->
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8"> <!--Numarul standard de caractere pe internet-->
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gestiunea Studentilor ERASMUS+</title>
    <!-- Bootstrap CSS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Style CSS-->
    <link rel="stylesheet" href="css/style.css">
    <!--Google Fonts-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap" rel="stylesheet">
  </head>
  <body>

<!--PRELOADER-->
<div class="loader_bg">
 <div class="loader">
   <img src="img/12.gif" class="poza">
 </div>
</div>
<!--SFARSIT PRELOADER-->

<!--TOP BAR-->
<div class="top-bar">
    <div class="container">
        <div class="col-12 text-right">
            <p><a href="https://biblioteca.tuiasi.ro/">University Library</a></p>
        </div>
    </div>
</div>
<!--SFARSIT TOP BAR-->

<!--TOP NAVIGATION-BAR-->
<nav class="navbar navbar-expand-sm sticky-top navbar-light bg-light"> <!--alegem culoarea light in cazul nostru-->
  <div class="container">
  
    <a href="index.php" class="navbar-brand">
      <img src="img\logo1.png" alt="Logo" title="Logo">
    </a>

    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#Sticky-nav"> <!--facem butonul ca sa il putem apasa si sa ne ofere informatia vertical in jos-->
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="Sticky-nav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item"><a href="https://campus.tuiasi.ro/en/home/" class="nav-link active">TUIASI Campus</a></li>
        <li class="nav-item"><a href="https://www.tuiasi.ro/?lang=en" class="nav-link">University Page</a></li>
      </ul>
      </div>
  </div>
</nav>
<!--SFARSIT TOP NAVIGATION-BAR-->

<!--SCROLL INDICATOR-->
  <div class="progress-bar sticky-top" role="progressbar" id="myBar"></div>
  </div>
<!--SFARSIT SCROLL INDICATOR-->

<!--MOVING IMAGE CAROUSEL-->
<div id="carousel" class="carousel slide" data-ride="carousel" data-interval="3500">

  <!--CAROUSEL CONTENT-->
  <div class="carousel-inner">
  
    <div class="carousel-item active">
      <img src="img/carousel/1.jpg" alt="" class="w-100">
  
    </div>
   
    <div class="carousel-item">
      <img src="img/carousel/2.jpg" alt="" class="w-100">
      
    </div>
  
  </div>
<!--SFARSIT CAROUSEL CONTENT-->

<!--CAROUSEL BUTTON (NEXT-PREVIOUS)-->
<a href="#carousel" class="carousel-control-prev" role="button" data-slide="prev">
  <span class="fa fa-caret-left fa-4x"></span>
</a>

<a href="#carousel" class="carousel-control-next" role="button" data-slide="next">
  <span class="fa fa-caret-right fa-4x"></span>
</a>
<!--SFARSIT PREVIOUS & NEXT BUTTON-->
</div>
<!--SFARSIT IMAGE CAROUSEL-->

<!--LINIA DE LA INTERNATIONAL STUDENTS-->
<div class="col-12 text-center mt-5">
  <h1 class="text-dark pt-4">International Students</h1>
  <div class="border-top border-primary w-25 mx-auto my-3"></div>
</div>
<!--SFARSIT INTERNATIONAL-->

<!--MAIN PAGE TESTIMONIALE-->
<div class="container-fluid padding">
  <div class="row padding">
    <div class="col-md-4">
      <div class="card">
        <img class="card-img-top" src="img/carousel/4.jpg">
        <div class="card-body">
          <h4 class="card-title">‘It was great to go into a community and really be recognised as part of a community.’</h4>
          <p1 class="card-text">Raluca Chirita <br>Electrical Engineer</p1>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="card">
        <img class="card-img-top" src="img/carousel/3.jpg">
        <div class="card-body">
          <h4 class="card-title">‘I became more mature and confident as a result of the [Erasmus] experience. I am definitely more open to other cultures.’</h4>
          <p class="card-text">Alin Bidirliu <br>Electrical Engineer</p>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="card">
        <img class="card-img-top" src="img/carousel/6.jpg">
        <div class="card-body">
          <h4 class="card-title">‘The Erasmus experience made me feel like a global citizen. I’m at home in any place in the world.’</h4>
          <p class="card-text">Alexandru Baban <br>Electrical Engineer</p>
        </div>
      </div>
    </div>

  </div>
</div>
<!--SFARSIT 3 COLOANE TESTIMONIALE-->

<!--DISCOVER OUR UNIVERSITY-->
<section class="about section mt-5">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6 text-center heading-section mb-5">
        <h2 class="text-black mb-2">Welcome to our University</h2>
        <div class="border-top border-primary w-25 mx-auto my-3"></div>
        <h4 class="text-black mb-2">Prestige and tradition</h4>
      </div>
    </div>
  </div>
</section>
<!--SFARSIT DISCOVER OUR CITY-->

<!--INFORMATII DESPRE UNIVERSITATE-->
<div class="timeline">
  <div class="clasa left">
    <div class="content">
      <h2>The most beautiful library in the world</h2>
      <p>The Iasi Technical University’s Library has been selected among the most beautiful 25 libraries in the world, according to an online survey initiated by the Boredpanda.com portal. Today, (the 18th of January) the library is on the first place in the site visitor’s preference top. In“competition” there are architectural masterpieces such as the Trinity College Library in Dublin, the Royal Portuguese Library in Rio de Janeiro, the National Library in Prague or the National Library of France.</p>
    </div>
  </div>
  <div class="clasa right">
    <div class="content">
      <h2>Online Orientation Day – TUIASI international students’ first steps into our university</h2>
      <p>54 incoming students from Portugal, Spain, Italy, Morocco and Belgium were greeted Tuesday, 15 October 2019, at the Erasmus Welcome Day event, held at FabLab, by the International Relations Office team, their assigned Buddy System students and Doina Sîrbu, TUIASI alumni, representing Erasmus Student Network (ESN).</p>
    </div>
  </div>
  <div class="clasa left">
    <div class="content">
      <h2>Buddy System – TUIASI Romanian students will help international incoming students get settled in</h2>
      <p>serve as a contact person once the international student is enrolled at the university
keep in contact via e-mail and / or social media with the international student, providing useful info about Iași and the university
help the international student at his or her arrival and take them to the International Office
take part in other university activities aimed at incoming students
provide online / offline help to incoming students with whatever problems may arise</p>
    </div>
  </div>
  <div class="clasa right">
    <div class="content">
      <h2>Erasmus Welcome Day</h2>
      <p>International Relations staff, led by Professor Irina Lungu, informed the incoming students on how to better prepare themselves for their new life in Iași. They learned about the university and its faculties, Tudor Vladimirescu Student Campus, Iași public transit and student facilities, the academic calendar and pandemic Survivor Kit, the Erasmus Student Network organisation and there was also a Q&A session.</p>
    </div>
  </div>
</div>
<!--SFARSIT INFORMATII UNIVERSITATE-->

<!--Footer-->
<section id="footer">
  <div class="container">
    <div class="row text-center text-xs-center text-sm-left text-md-left">
      <div class="col-xs-12 col-sm-4 col-md-4">
        <h5>Schools</h5>
        <ul class="list-unstyled quick-links">
          <li><a href="https://arh.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Arhitecture</a></li>
          <li><a href="https://ac.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Automatic Control & Computer Engineering</a></li>
          <li><a href="https://icpm.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Chemical Engineering & Environmental Protection</a></li>
          <li><a href="https://ci.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Civil Engineering & Building Services</a></li>
          <li><a href="https://cmmi.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Machine Manufacturing & Industrial Management</a></li>
          <li><a href="https://etti.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Electronics, Telecommunications & Information Technology</a></li>
          <li><a href="https://ieeia.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Electrical Engineering</a></li>
          <li><a href="https://hgim.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Hydrotechnics, Geodesy & Environmental Engineering</a></li>
          <li><a href="https://mec.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Mechanical Engineering</a></li>
          <li><a href="https://sim.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Material Science & Engineering</a></li>
          <li><a href="https://dima.tuiasi.ro/"><i class="fa fa-angle-double-right"></i>Textiles, Leather & Industrial Management</a></li>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <h5>Students</h5>
        <ul class="list-unstyled quick-links">
          <li><a href="https://campus.tuiasi.ro/en/cafeteria/"><i class="fa fa-angle-double-right"></i>Canteen</a></li>
          <li><a href="https://campus.tuiasi.ro/en/home/"><i class="fa fa-angle-double-right"></i>University Campus</a></li>
          <li><a href="https://campus.tuiasi.ro/studenti-senatori/"><i class="fa fa-angle-double-right"></i>Senator students</a></li>
          <li><a href="https://www.tuiasi.ro/discover-tuiasi/facilities-campus/?lang=en"><i class="fa fa-angle-double-right"></i>Facilities & Campus</a></li>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <h5>Things to do in Iasi</h5>
        <ul class="list-unstyled quick-links">
          <li><a href="http://iasi.travel/ro/"><i class="fa fa-angle-double-right"></i>Discover Iasi</a></li>
          <li><a href="https://www.tuiasi.ro/discover-tuiasi/traveling-to-from-iasi/?lang=en"><i class="fa fa-angle-double-right"></i>Traveling to / from Iasi</a></li>
          <li><a href="https://www.tripadvisor.com/Tourism-g304060-Iasi_Iasi_County_Northeast_Romania-Vacations.html"><i class="fa fa-angle-double-right"></i>Things to do in Iasi</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
        <ul class="list-unstyled list-inline social text-center">
          <li class="list-inline-item"><a href="https://www.facebook.com/TUIasiRO/"><i class="fa fa-facebook"></i></a></li>
          <li class="list-inline-item"><a href="https://www.linkedin.com/school/universitatea-tehnic%C4%83-%E2%80%9Egh.-asachi%E2%80%9D-din-ia%C8%99i/"><i class="fa fa-linkedin"></i></a></li>
          <li class="list-inline-item"><a href="https://www.instagram.com/tuiasi.ro/?fbclid=IwAR2VhJaacu_zDvR0mZwVXgUTGPIZt9PI0eAo_8X9-FlIGzcG--2jHemEtvA"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
      <hr>
    </div>	
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
        <p>Romania</p>
        <p class="h6">© All right Reversed<br>Gheorghe Asachi Technical University of Iaşi.</p>
      </div>
      <hr>
    </div>	
  </div>
</section>
<!--SFARSIT Footer-->

<!--Ajax-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!--Boostrap.min.js-->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!--JQuery-->
<script src="js/jquery-3.4.1.js" ></script>
<!--Boostrap JS-->
<script src="js/bootstrap.min.js"></script>
<!--Popper JS-->
<script src="js/popper.min.js"> </script>
<!--Font Awesome-->
<script src="js/all.min.js"></script>
<script src="https://kit.fontawesome.com/39b04b2713.js" crossorigin="anonymous"></script>
<!--Preloader-->
<script src="https://cdnj.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
setTimeout(function(){
  $(".loader_bg").fadeToggle();
}, 1200);
</script>
<!--SCROLL INDICATOR-->
<script>
window.onscroll = function() {myFunction()};
function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}
</script>
  </body>
</html>